package Licencias;

public class LicenciaVehiculos {

    //LICENCIA CON HERENCIA PARTICULARES 
    protected String nombreyApellido;
    protected String nacionalidad;
    protected String dni;
    protected String fechaNacimiento;
    protected String autorizacion;
    protected String caducidad;

    @Override
    public String toString() {
        return "LicenciaVehiculos{" + "nombreyApellido=" + nombreyApellido + ", nacionalidad=" + nacionalidad + ", dni=" + dni + ", fechaNacimiento=" + fechaNacimiento + ", autorizacion=" + autorizacion + ", caducidad=" + caducidad + '}';
    }


}
